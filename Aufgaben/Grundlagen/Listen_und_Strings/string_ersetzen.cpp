#include<iostream>
#include<string>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
 * Schreiben Sie eine Funktion `replace()`, die als Argumente einen String s
 * und zwei Buchstaben c1 und c2 erwartet. Die Funktion soll jedes Vorkommen
 * von c1 in s durch c2 ersetzen und das Ergebnis zurückliefern.
 */
string replace(string s1, char c1, char c2) {
    // Geben Sie hier Ihre Loesung ein.
    return false;
}

/*** TESTCODE/MAIN ***/
int main()
{
    cout << replace("Hallo", 'a', 'b') << endl;    // Soll "Hbllo" ausgeben.
    cout << replace("Hallo", 'l', 'x') << endl;    // Soll "Haxxo" ausgeben.
    cout << replace("Hallo", 'H', 'h') << endl;    // Soll "hallo" ausgeben.
    cout << replace("Hallo", 'x', 'a') << endl;    // Soll "Hallo" ausgeben.
}


/*** Komplexere Variante:
 * Akzeptieren Sie statt des zweiten Buchstabens einen ganzen String.
 *
 * Beispiel: replace("Hallo", 'o', " of Fame") bildet "Hallo" auf "Hall of Fame" ab.
 */

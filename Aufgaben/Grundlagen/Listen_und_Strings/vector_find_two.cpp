#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Suchen von Elementen in einem Vektor ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `find_two()`, die als Argument einen Vektor `v` aus Zahlen und eine einzelne Zahl `x` erwartet.
Die Funktion soll die erste Position `v` bestimmen, an der `x` zwei Mal hintereinander steht. Gibt es keine solche Position, soll die Länge von `v` geliefert werden.
***/
int find_two(vector<int> v, int x) {
    // ...
    return 0;
}

/*** TESTCODE/MAIN ***/
int main()
{
    vector<int> v1 = { 1, 3, 5, 5, 7, 9, 11 };
    
    cout << find_two(v1, 5) << endl;   // Soll '2' ausgeben.
    cout << find_two(v1, 3) << endl;   // Soll '7' ausgeben.
    cout << find_two(v1, 42) << endl;  // Soll '7' ausgeben.
}

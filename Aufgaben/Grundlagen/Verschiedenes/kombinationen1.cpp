#include<iostream>
#include<string>
#include<vector>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion 'produkte', die einen vector<int>
    erwartet. Die Funktion soll alle Produkte von je zwei Zahlen im
    Vektor zurückliefern (als neuen Vektor).
 ***/
vector<int> produkte(vector<int> v)
{
    /* Ihre Loesung hier... */
}

/** Hilfsfunktion, gibt einen vector<int> auf der Konsole aus.*/
void print(vector<int> v)
{
    for (int el:v)
    {
        cout << el << " ";
    }
    cout << endl;
}

int main()
{
    vector<int> v1 = { 2,3,4 };
    vector<int> e1 = produkte(v1);
    print(e1);                     // Soll "4,6,8,9,12,16" ausgeben
    
    vector<int> v2 = { 1,3,5,7 };
    vector<int> e2 = produkte(v2);
    print(e2);                     // Soll "1,3,5,7,9,15,21,25,35,49" ausgeben
    
    return 0;
}



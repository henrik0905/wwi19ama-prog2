#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

/* Aufgabenstellung:
 
   Schreiben Sie eine Funktion find_gt(), die einen Vektor v aus
   Zahlen als Parameter sowie einen Wert x erwartet. Die Funktion
   soll das erste Element in v finden, das größer ist als x und
   dessen Position zurückliefern. Kommt x nicht vor, soll die
   Funktion die Länge von v zurückliefern.
 */
int find_gt(vector<int> v, int x)
{
    for (int i=0; i<v.size(); i++)
    {
        if (v[i] > x)
        {
            return i;
        }
    }
    return v.size();
    
    // Alternative Lösung:
    return std::distance(
      v.begin(),
      std::find_if(v.begin(), v.end(), [x](int el)
      {
        return el > x;
      }));
}

int main()
{
    std::cout << find_gt({1,3,5,7,9}, 3) << endl; // Soll 2 ausgeben
    std::cout << find_gt({1,3,5,7,9}, 2) << endl; // Soll 1 ausgeben
    std::cout << find_gt({1,3,5,7,9}, 6) << endl; // Soll 3 ausgeben
    std::cout << find_gt({}, 3) << endl;          // Soll 0 ausgeben
    
    return 0;
}

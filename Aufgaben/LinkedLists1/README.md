# Übungsaufgaben zu einfach verketteten Listen

In diesem Verzeichnis gibt es eine Reihe von Aufgaben zu verketteten Listen.

## Überblick über die Vorgabe:

Da die bereitgestellte Klasse mehrere Funktionen enthält, ist die Implementierung auf mehrere Dateien verteilt.

Es sind vier Dateien vorgegeben: `linkedlist.h`, `linkedlist.cpp`, `test.cpp` und `aufgaben.cpp`.
In den beiden ersten ist eine Implementierung von einfach verketteten Listen vorgegeben,
mit der Sie weiterarbeiten sollen.

- Der Header `linkedlist.h` enthält die Definition der Datentypen, die hier verwendet werden.
- Die Quelldatei `linkedlist.cpp` enthält die Implementierung der vorgegebenen
  Funktionen aus `linkedlist`. Nicht alle Funktionen der Liste sind schon implementiert.
- `test.cpp` bindet den Header `linkedlist.h` ein und benutzt/prüft die Funktionen der Liste.
- `aufgaben.cpp` enthält Aufgabenstellungen und Code-Vorgaben für die restlichen
  Funktionen aus `linkedlist.h`. Dies sind die zu bearbeitenden Aufgaben.
  In `test.cpp` werden auch diese Funktionen bereits getestet.

## Bauen:

### Direkt mittels der Konsole:

Um ein Projekt zu übersetzen, das aus mehreren Dateien besteht, müssen Sie beim
Compileraufruf alle Quelldateien (aber nicht die Header) angeben, die mit übersetzt
werden sollen. In diesem Fall wäre die Zeile für den Aufruf:

```g++ -o linkedlist linkedlist.cpp test.cpp```

Dieser Aufruf übersetzt die Dateien`linkedlist.cpp` und `test.cpp` und erzeugt dabei
die ausführbare Datei `linkedlist.exe` (bzw. nur `linkedlist` unter Linux und MacOS).

Falls Sie das Tool *CMake* installiert haben, können Sie auf der Konsole auch einfach
folgendes eingeben (wenn Sie im Projektverzeichnis sind):

```cmake --build .```

CMake ist ein Tool, das Abhängigkeiten und notwendige Dateien erkennt und den Bau eines
Projekts auslösen kann. Es ist Open Source und daher kostenlos verfügbar.
Wenn Sie z.B. Visual Studio oder CLion installiert haben, ist die Chance hoch, dass Sie
es schon haben.

### Als Projekt in Visual Studio oder CLion:

Diese beiden Entwicklungsumgebungen können direkt mit CMake-Projekten umgehen,
Sie sollten dort einfach diesen Code-Ordner als Projekt laden können und startklar sein.

### Anmerkung zu CMake:

Das Projekt wird über die Datei `cmakelists.txt` konfiguriert.
Wenn Sie weitere Quelldateien hinzufügen, müssen Sie die Zeile `add_executable(...)` anpassen.
Sie können auch mehrere solche Zeilen hinzufügen, damit definieren Sie dann verschiedene
sog. *Build-Targets*. D.h. verschiedene Executables und welche Quellen dazugehören.

Auch Visual Studio Code ist mittels CMake zumindest etwas leichter einzurichten.
Sie brauchen dafür separat einen Compiler (z.B. MinGW), CMake und natürlich Visual Studio Code
und dessen C++-Plugin. Als Build-Befehl für ein Projekt wählt man dann einfach den obigen
Build-Befehl `cmake --build .`. Es ist aber immer noch deutlich komplizierter als
Visual Studio oder CLion.

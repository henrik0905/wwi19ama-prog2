#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/** Beschreibung dieses Headers:
 *
 * Diese Header-Datei enthält die Definition einer Klasse für einfach verkettete Listen.
 *
 * Es werden nur die Klassen definiert und deren Member-Funktionen deklariert.
 * Die Implementierungen der Funktionen liegen in der Datei linkedlist.cpp.
 *
 * Es werden zwei Record-Datentypen definiert, einmal Element für die einzelnen
 * Elemente der Liste und einmal LinkedList für die eigentliche Liste.
 * Element ist relativ einfach, es enthält jeweils nur ein Datum und einen Pointer
 * auf das nächste Element.
 * LinkedList enthält sämtliche Funktionen, die für den Zugriff auf bzw. die Arbeit mit
 * der Liste notwendig sind.
 */

// Benötigte Includes
#include<iostream>
#include<string>

/// Datentyp für Elemente der Liste
struct Element {
    int data;
    Element * next;

    // Konstruktor für Element
    Element();
    
    // Gibt zurück, ob dieses Element ein Dummy-Element ist.
    bool isDummy();
};

/// Datentyp für die eigentliche Liste
struct LinkedList {
    /// Pointer auf das erste Element der Liste
    Element * head;
    
    /// Konstruktor: Wird beim Erzeugen einer Variable
    /// vom Typ LinkedList aufgerufen und initialisiert die Liste.
    LinkedList();
    
    /// Fügt ein neues Element am Ende der Liste an.
    void push_back(int x);
    
    /// Entfernt das letzte Element der Liste.
    void pop_back();
    
    /// Fügt ein neues Element an Stelle pos ein.
    void insert(int pos, int x);
    
    /// Entfernt das Element an Stelle pos aus der Liste.
    void erase(int pos);
    
    /// Vertauscht die Elemente an den angegebenen Stellen.
    void swap(int pos1, int pos2);
    
    /// Sucht das erste Element mit dem Datum x und gibt dessen Position zurück.
    /// Liefert die Länge der Liste, wenn x nicht vorkommt.
    int find(int x) const;
    
    /// Liefert die Länge der Liste zurück
    int size() const;
    
    /// Liefert eine String-Repräsentation der Liste.
    std::string str() const;
};

/// Stream-Ausgabe-Operator für den Listen-Datentyp
/// Dies ermöglicht es, eine Liste direkt mittels cout auszugeben, wie sonst auch bei int, string etc.
/// Siehe dazu die Verwendung in test.cpp.
std::ostream & operator<<(std::ostream & left, LinkedList const & right);

#endif

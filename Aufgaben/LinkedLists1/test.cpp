/** Main-Funktion und Tests für die Klasse LinkedList.
 *
 * Wir definieren hier eine Reihe von Testfunktionen für die Liste.
 * Jede der Testfunktionen definiert eine oder mehrere Beispiellisten
 * und ruft dann Funktionen darauf auf, um zu prüfen, ob alles funktioniert.
 *
 * Wir achten dabeidarauf , dass in jeder Funktion wirklich
 * nur die zu testende Funktion benutzt wird.
 */

#include"linkedlist.h"

#include <iostream>
#include <initializer_list>
using namespace std;

// Vorausdeklaration der Testfunktionen, Implementierung s.u.
void test_LinkedList_pop_back();
void test_LinkedList_erase();
void test_LinkedList_swap();
void test_LinkedList_find();
void test_LinkedList_size();

int main()
{
    test_LinkedList_pop_back();
    test_LinkedList_erase();
    test_LinkedList_swap();
    test_LinkedList_find();
    test_LinkedList_size();

    return 0;
}

// Hilfsfunktion: Erzeugt eine Liste aus den angegebenen Zahlen.
LinkedList createList(initializer_list<int> inits)
{
    LinkedList result;
    Element * last = result.head;

    for (auto el : inits)
    {
        last->data = el;
        last->next = new Element();
        last = last->next;
    }

    return result;
}

void test_LinkedList_pop_back()
{
    LinkedList l = createList({1,3,5});

    // Lösche letztes Element.
    l.pop_back();

    cout << l << endl;                 // Soll "1 3" ausgeben.

    // Lösche drei weitere Elemente
    l.pop_back();
    l.pop_back();
    l.pop_back();

    cout << l << endl;                 // Soll "" ausgeben.
}

void test_LinkedList_erase()
{
    LinkedList l = createList({1,3,5});

    // Lösche mittleres Element.
    l.erase(1);

    cout << l << endl;                 // Soll "1 5" ausgeben.

    // Lösche zwei nicht existierende Elemente.
    l.erase(-1);
    l.erase(15);

    cout << l << endl;                 // Soll "1 5" ausgeben.
}

void test_LinkedList_swap()
{
    LinkedList l = createList({1,3,5});

    // Vertausche erstes und zweites Element.
    l.swap(0,1);

    cout << l << endl;                 // Soll "5 1" ausgeben.

    // Vertausche zwei nicht existierende Elemente.
    l.swap(0,1);

    cout << l << endl;                 // Soll "5 1" ausgeben.
}

void test_LinkedList_find()
{
    LinkedList l = createList({1,3,5});

    cout << l.find(3) << endl;                 // Soll "1" ausgeben.
    cout << l.find(42) << endl;                 // Soll "3" ausgeben.
}

void test_LinkedList_size()
{
    LinkedList l1 = createList({1,3,5});
    cout << l1.size() << endl;                      // Soll "3" ausgeben.

    LinkedList l2 = createList({});
    cout << l2.size() << endl;                      // Soll "0" ausgeben.
}

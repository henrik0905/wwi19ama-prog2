
/** Beschreibung dieser Quelldatei:
 *
 * Diese Datei enthält die Aufgabenstellungen sowie Vorlagen
 * zu den Funktionen, deren Implementierung in linkedlist.cpp offen geblieben ist.
 *
 * Alle Funktionen in dieser Datei sind so vorgegeben, dass sie übersetzt werden
 * können. Ansonsten wäre test.cpp unbenutzbar, solange nicht alles implementiert ist.
 * Wenn Sie die Signaturen der Funktionen verändern wollen, können Sie das machen.
 * Allerdings müssen Sie darauf achten, dass Sie dann auch die entsprechenden Definitionen
 * in linkedlist.h sowie die Verwendugsstellen in test.cpp anpassen.
 */

// Benötigte Includes
#include"linkedlist.h"

void LinkedList::pop_back()
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;
    
    // Den Zeiger verschieben, bis er auf das vorletzte Element zeigt.
    // D.h. auf das Element, dessen Nachfolger der Dummy ist.
    
    // Dann das letzte Element (den Dummy) löschen (mit delete) und
    // den Next-Pointer von current auf nullptr setzen. Das macht
    // current zum Dummy.
}

void LinkedList::erase(int pos)
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;
    
    // Den Zeiger verschieben, bis er auf das Element vor dem zu
    // löschenden zeigt. Dann den Next-Pointer von current
    // auf das übernächste Element umbiegen.
    
    
    // Achtung: Denken Sie daran, das gelöschte Element mit
    // delete zu löschen.
    // Dazu sollten Sie einen der Pointer auf das zu löschende
    // Element oder auf dessen Nachfolger zwischenspeichern.
}

void LinkedList::swap(int pos1, int pos2)
{
    // Suchen Sie analog zu oben nach den Vorgängern der beiden
    // zu tauschenden Elemente und biegen Sie Pointer um.
}

int LinkedList::find(int x) const
{
    // Suchen Sie das erste Vorkommen von x und zählen Sie mit,
    // wie viele Schritte Sie dafür gehen müssen.
    
    return 0;
}

int LinkedList::size() const
{
    // Suchen Sie den Dummy und zählen Sie die Schritte auf dem Weg.
    
    return 0;
}


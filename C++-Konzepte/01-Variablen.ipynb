{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variablen und Datentypen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deklarieren von Variablen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variablen werden *deklariert*, indem man ihren Typ voranstellt und anschließend den Namen der Variablen schreibt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int i;\n",
    "float f;\n",
    "char c;\n",
    "bool b;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Anschließend kann man Werte mittels `=` an die Variablen zuweisen und sie wieder benutzen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i = 42;\n",
    "f = 27.5;\n",
    "c = i + 2 * f;\n",
    "c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hier haben wir die Werte von `i` und `f` miteinander verrechnet.\n",
    "Der berechnete Wert ist 97, was der ASCII-Code des Buchstaben 'a' ist.\n",
    "Diesen Wert haben wir in c gespeichert."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** In Jupyter-Notebooks mit C++-Kernel kann man eine Variable ohne Semikolon ans Ende einer Zelle schreiben, um ihren Wert ausgeben zu lassen. Dies ist eine Jupyter-Spezialität, im Normalfall muss man jede Anweisung mit einem Semikolon beenden und Ausgaben erfolgen z.B. mittels `cout` (siehe [Notebook zu Ein-/Ausgabe](04_Ein-Ausgabe.ipynb))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialisierung von Variablen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variablen haben in C++ nach ihrer Initialisierung i.d.R. erstmal einen zufälligen Wert. Sie werden nicht initialisiert, d.h. sie enthalten das, was an dieser Speicherstelle gerade steht. Meistens wird das 0 sein, aber dafür gibt es keine Garantie."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Damit Variablen sicher verwendet werden können, müssen sie vor dem ersten Lesezugriff einen Wert bekommen. Oben haben wir das für `i`, `f` und `c` gemacht, nicht jedoch für `b`.\n",
    "\n",
    "Die Initialisierung kann auch direkt bei der Deklaration erfolgen, das nennt man dann die *Definition* einer Variablen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int x = 0;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Datentypen in C++"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Es folgt ein kurzer Überblick über einige der wichtigsten Datentypen in C++.\n",
    "Die Bit-Angaben beziehen sich auf den typischen 64-Bit X86-Prozessor, auf anderen Plattformen können die Angaben abweichen.\n",
    "\n",
    "Es sind immer Definitionen angegeben, um klarzumachen, wie die typischen Literale für den Datentyp aussehen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ganzzahlige Datentypen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int y = 0;          // 32 Bit Integer mit Vorzeichen\n",
    "long int l = 0;     // 64 Bit Integer mit Vorzeichen\n",
    "char d = 'a';       //  8 Bit Integer ohne Vorzeichen, steht für Buchstaben.\n",
    "unsigned int u = 0; // 32 Bit Integer ohne Vorzeichen\n",
    "uint32_t v = 0;     // 32 Bit Integer ohne Vorzeichen\n",
    "size_t st = 0;      // 64 Bit Integer ohne Vorzeichen, meist für Längen benutzt.\n",
    "bool b2 = true;     // Kann die Werte true und false haben, ist aber meist wenigstens ein Byte"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** Man kann den Speicherverbrauch einer Variable mittels der Funktion `sizeof()` bestimmen. Dies gilt allerdings nicht für Pointer- und Container-Datentypen, siehe unten. `sizeof()` liefert die Anzahl an Bytes, die eine Variable verbraucht:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sizeof(u)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fließkomma-Datentypen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "float g = 4.2;   // 32 Bit Fließkomma-Variable\n",
    "double h = 3.8   // 64 Bit Fließkomma-Variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pointer und Referenzen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int * pi = nullptr;  // Pointer auf ein Int\n",
    "float * pf = &g;     // Pointer auf ein Float"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pointer sind Variablen, die die Speicheradresse einer anderen Variable enthalten.\n",
    "In diesem Beispiel ist `pi` ein *Nullpointer*, d.h. er zeigt auf keine andere Variable.\n",
    "`pf` dagegen hat bei seiner Initialisierung die Adresse von g (in der Form `&g`) bekommen.\n",
    "Auf 64-Bit-Architekturen haben Pointer immer 64 Bit Länge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int & r = y;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hier wurde eine *Referenz* auf `y` definiert. Referenzen sind neue Namen für bereits bestehende Variablen.\n",
    "Die Referenz `r` kann jederzeit an Stelle von `y` verwendet werden.\n",
    "Am Häufigsten kommen Referenzen als Funktionsparameter vor (s. [Notebook zu Funktionen](03_Funktionen.ipynb)),\n",
    "um Kopieraufwand bei großen Variablen zu vermeiden."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int a1[5] = {2,4,6,8,10}; // Ein Integer-Array der Länge 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arrays werden deklariert, indem man den Basisdatentyp und den Namen hinschreibt, gefolgt von eckigen Klammern, in denen die Größe steht.\n",
    "Arrays können wie oben mit geschweiften Klammern initalisiert werden. In dem Fall, kann man die Größenangabe auch weglassen, weil sie aus dem Kontext klar wird:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int a2[] = {1,3,5};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Auf die Elemente eines Arrays kann mittels des Operators `[]` zugegriffen werden:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a2[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** Die Funktion `sizeof()` liefert für ein Array nicht immer die Länge des Arrays, weil Arrays intern eigentlich Pointer-Typen sind.\n",
    "Für die obigen beiden Arrays liefert `sizeof()`die korrekte Länge. Allerdings ist das z.B. bei Arrays, die als Funktionsparameter übernommen werden, nicht immer der Fall. Man sollte sich also nicht auf `sizeof()` verlassen, um die Länge eines Arrays zu bestimmen. Insgesamt ist `std::vector` (s.u.) der bessere Array-Datentyp."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arrays können auch direkt als Pointer erzeugt werden:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int * ptrarray = new int(8);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dies erzeugt einen int-Pointer und reserviert Speicher für 8 Elemente. Der Pointer zeigt anschließend auf diesen reservierten Speicher.\n",
    "Dabei ist allerdings zu beachten, dass zwar `ptrarray` eine lokale Variable ist, der reservierte Speicher aber nicht.\n",
    "D.h. wenn ein Pointer innerhalb einer Funktion so definiert wird, dann wird am Ende der Funktion der Pointer ungültig, der Speicher\n",
    "bleibt aber reserviert. Das ist dann ein *Speicherleck*, ein Stück Speicher, dessen Adresse niemand mehr kennt, das also nicht mehr nutzbar ist.\n",
    "\n",
    "Reservierter Speicher kann mit `delete` wieder freigegeben werden:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delete ptrarray;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Anmerkung:** Während Pointer durchaus relevant sind, ist das Verwalten von Speicher mit `new` und `delete` ein fortgeschrittenes Konzept, das zudem in den meisten Programmen nicht notwendig sein sollte. Insbesondere wird dies in Container-Datentypen u.Ä. gemacht. Wir werden es bei Listen- und Baumtadentypen nutzen, in normalem Produktivcode sollte es i.d.R. aber nicht vorkommen. Stattdessen sind Container-Datentypen wie `std::vector`, `std::string` oder Smart-Pointer wie `std::unique_ptr` zu bevorzugen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Zeichenketten (Strings):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include<string>  // Include-Direktive, damit der Datentyp `string` bekannt ist"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "char s1[] = \"Hallo\";       // Ein Nullterminierter String\n",
    "std::string s2 = \"Welt\";   // Ein C++-String"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Es gibt i.W. zwei Arten von Strings, einmal ganz klassische Strings, die i.W. `char`-Arrays sind.\n",
    "Die Variable `s1` ist so ein String, er enthält die Zeichen 'H','a','l','l','o' und '\\0'.\n",
    "Also die Zeichen, die in der Initialisierung stehen und ein weiteres, die so. *terminierende Null*.\n",
    "Dies ist ein spezielles ASCII-Zeichen, das das Ende eines Strings markiert.\n",
    "\n",
    "In C++ gibt es zusätzlich die Klasse `std::string`, die einen erheblich bequemeren Umgang mit Strings ermöglicht.\n",
    "Eine präzise Beschreibung gibt es z.B. bei [cppreference.com](https://en.cppreference.com/w/cpp/string/basic_string)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Container-Datentypen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Includes, damit `std::vector` und `std::map` bekannt sind.\n",
    "#include<vector>\n",
    "#include<map>\n",
    "using namespace std; // Damit wir nicht jedes Mal std:: davor schreiben müssen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vector<int> v1 = {1,3,5,7,9};\n",
    "vector<float> v2 = {3.0,4,2};\n",
    "vector<int> v3;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ein Vektor ist eine Form des Arrays, die allerdings zusätzlich ein Speichermanagement mitbringt.\n",
    "Man kann einen Vektor wie ein Array benutzen, man kann aber z.B. mittels `push_back()` neue Elemente anhängen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v3.push_back(42);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v3[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Funktion `push_back()` hat ein neues Element angehängt. Sie kümmert sich ggf. auch darum, das Array zu verlängern.\n",
    "Die Klasse `vector` bringt eine Reihe an Member-Funktionen mit, z.B. eine Funktion `size()`, die die Länge liefert:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v3.size()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Funktion `sizeof()` würde im Gegensatz dazu nicht die korrekte Länge liefern:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sizeof(v3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dies liegt daran, dass ein `std::vector` kein reines Array, sondern ein sog. Container-Datentyp ist: Eine Klasse, die ein Array enthält (als Pointer), aber auch noch weitere Variablen. Insgesamt sind dies 24 Byte, nämlich ein Pointer und zwei `size_t`-Variablen, in denen die aktuelle Länge und die maximal zur Verfügung stehende Länge stehen. Mehr dazu in der Vorlesung."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Klassen `std::vector` und `std::string` stellen sehr ähnliche Schnittstellen zur Verfügung. Mehr dazu bei [cppreference.com](https://en.cppreference.com/w/cpp/container/vector)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ein weiterer wichtiger Container-Datentyp ist `std::map`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "map<string,string> dict;  // Eine Abbildung von Strings auf (andere?) Strings\n",
    "map<string,int> ages;     // Eine Abbildung von Strings auf Int"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Maps können wir uns als verallgemeinerte Arrays vorstellen: Während Arrays immer Zahlen auf Inhalte abbilden,\n",
    "können Maps beliebige Inhalte aufeinander abbilden. Dies ist bspw. für Wörterbücher oder andere komplexere Datensätze interessant:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dict[\"Auto\"] = \"Car\";\n",
    "dict[\"Baum\"] = \"Tree\";\n",
    "ages[\"Max Mustermann\"] = 42;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dict[\"Auto\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ages[\"Max Mustermann\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ages[\"Arno Nym\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mehr zu `std::map` wieder bei [cppreference.com](https://en.cppreference.com/w/cpp/container/map)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

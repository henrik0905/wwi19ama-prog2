#include <iostream>
using namespace std;

struct Array {
  int laenge = 0;
  int max_laenge = 0;
  int * daten = nullptr;

  Array()
  {
    daten = new int[5]; // Erzeugt ein Array der Länge 5
    max_laenge = 5;
  }

  void push_back(int x)
  {
    if(laenge == max_laenge)
    {
      int * neue_daten = new int[max_laenge * 2];
      for (int i=0; i<max_laenge; i++)  // Alle Elemente kopieren
      {
        neue_daten[i] = daten[i];  // Element i vom alten ins neue kopieren
      }
      delete daten;
      daten = neue_daten;  // Daten-Pointer auf das neue Array zeigen lassen.
      max_laenge *= 2;  // max_laenge = max_laenge * 2

    }
    daten[laenge] = x;
    laenge++;
    
  }
};


int main() {
  Array a;

  // a.laenge == 0
  // a.max_laenge == 5
  a.push_back(13);

  // a.laenge == 1
  // a.max_laenge == 5
  a.push_back(3);
  a.push_back(3);
  a.push_back(3);
  a.push_back(3);

  a.push_back(3);

  cout << a.daten << endl;


  
}
